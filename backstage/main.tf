terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>3.99.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "~> 2.12.0"
    }
    kubectl = {
      source  = "alekc/kubectl"
      version = "~> 2.0"
    }
  }
}


resource "kubectl_manifest" "namespace" {
  yaml_body = <<YAML
apiVersion: v1
kind: Namespace
metadata:
  name: backstage
YAML
}

resource "kubectl_manifest" "secret" {
  yaml_body  = <<YAML
apiVersion: v1
kind: Secret
metadata:
  name: postgres-secrets
  namespace: backstage
type: Opaque
data:
  POSTGRES_USER: ${base64encode("psqladmin")}
  POSTGRES_PASSWORD: ${base64encode(var.postgress_password)}
YAML
  depends_on = [kubectl_manifest.namespace]
}

resource "kubectl_manifest" "backstage" {
  yaml_body = templatefile("${path.module}/templates/backstage.tftpl", {
  })
  depends_on = [kubectl_manifest.secret]

}
