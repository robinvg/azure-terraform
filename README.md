## Devcontainer

Installeer een Docker variant.
Installeer de vscode extension Dev Containers.
Open vscode opnieuw en klik rechtonder op Reopen in Container.
![alt text](image.png)

Dit duurt de eerste keer een aantal minuten

## Handmatig

installeer terraform en azure-cli

https://developer.hashicorp.com/terraform/install
https://learn.microsoft.com/nl-nl/cli/azure/install-azure-cli
